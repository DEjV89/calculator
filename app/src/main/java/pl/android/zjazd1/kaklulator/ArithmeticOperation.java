package pl.android.zjazd1.kaklulator;

/**
 * Created by dkrezel on 2015-04-27.
 */
public enum ArithmeticOperation {

	ADD(0, "A"), SUBSTRACT(1, "S"), MULTIPLE(2, "M"), DIVIDE(3, "D");

	private int code;
	private String name;

	ArithmeticOperation(int code, String name) {
		this.code = code;
		this.name = name;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
