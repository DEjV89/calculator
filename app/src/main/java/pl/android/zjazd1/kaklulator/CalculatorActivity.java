package pl.android.zjazd1.kaklulator;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import pl.android.zjazd1.kaklulator.R;

/**
 * Created by dkrezel on 2015-04-26.
 */
public class CalculatorActivity extends ActionBarActivity {

	private static final String EMPTY = "";

	private EditText mText1;
	private EditText mText2;

	private Button mButtonAdd;
	private Button mButtonSubtraction;
	private Button mButtonMultiple;
	private Button mButtonDivide;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.calculator_activity);

		mText1 =  findView(R.id.text1);
		mText2 =  findView(R.id.text2);

		implementButtonsListeners();
	}

	private void implementButtonsListeners() {
		mButtonAdd = findView(R.id.button_add);
		mButtonMultiple = findView(R.id.button_multiple);
		mButtonSubtraction = findView(R.id.button_subtraction);
		mButtonDivide = findView(R.id.button_divide);


		mButtonAdd.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				arithmeticOperation(ArithmeticOperation.ADD);
			}
		});

		mButtonSubtraction.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				arithmeticOperation(ArithmeticOperation.SUBSTRACT);
			}
		});

		mButtonMultiple.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				arithmeticOperation(ArithmeticOperation.MULTIPLE);
			}
		});

		mButtonDivide.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				arithmeticOperation(ArithmeticOperation.DIVIDE);
			}
		});
	}

	private void arithmeticOperation(ArithmeticOperation mOperation) {
		String value1 = mText1.getText().toString();
		String value2 = mText2.getText().toString();
		if (value1 == null || EMPTY.equals(value1) || value2 == null || EMPTY.equals(value2)) {
			return;
		}

		int int1 = Integer.parseInt(value1);
		int int2 = Integer.parseInt(value2);
		performOperation(mOperation, int1, int2);
	}

	private void performOperation(ArithmeticOperation mOperation, int mValue1, int mValue2) {
		switch (mOperation) {
			case ADD:
				Toast.makeText(this, mValue1 + mValue2 + EMPTY, Toast.LENGTH_LONG).show();
				break;
			case SUBSTRACT:
				Toast.makeText(this, mValue1 - mValue2 + EMPTY, Toast.LENGTH_LONG).show();
				break;
			case MULTIPLE:
				Toast.makeText(this, mValue1 * mValue2 + EMPTY, Toast.LENGTH_LONG).show();
				break;
			case DIVIDE:
				if (mValue2 == 0) {
					break;
				}
				Toast.makeText(this, mValue1 / mValue2 + EMPTY, Toast.LENGTH_LONG).show();
				break;
		}
	}

	private <T> T findView(int viewID) {
		return (T) findViewById(viewID);
	}
}
